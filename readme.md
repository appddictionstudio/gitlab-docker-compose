# GitLab Community Edition with Docker-Compose
Use this repo to stand up a GitLab Community Edition

## Setup Volumes
Setup the volumes:
```
docker volume create gitlab-data
docker volume create gitlab-config
docker volume create gitlab-logs
docker volume create gitlab-runner-config
```

## Setup Environment
After creation you can run the following:
```
docker-compose up -d
```
-d is for running it in de-attached mode. 

## Tear Down Environment
```
docker-compose down
```